# This method is to read spreadsheet
def read_spreadsheet
    files = File.join('**', '*.xl*')
    @excel_files = Dir.glob(files)
    if @excel_files.first
        Spreadsheet.open(@excel_files.first) do |book|
            book.worksheet(0).each do |row|
                @spreadsheets << row
            end
        end
    end
end

# This is to Create, Read and Write on Excel Spreadsheet
def make_spreadsheet
    if File.exist? @file
        Spreadsheet.client_encoding = 'UTF-8'
        open_book = Spreadsheet.open @file
        @spreadsheets.each do |a|
            break if a.include?("2. RECEITA DA COMISSÃO PELA VENDA DE VALE-TRANSPORTE")
            new_row_index = open_book.worksheet(0).last_row_index + 1
            p "Inserting at this position: #{new_row_index}"
            open_book.worksheet(0).insert_row(new_row_index, a)
        end
        File.delete @file
        open_book.write @file
    else
        Spreadsheet.client_encoding = 'UTF-8'
        @book = Spreadsheet::Workbook.new
        @book.create_worksheet name: 'year-data'
        @book.worksheet(0).insert_row 0, @header_columns
        @book.write @file
    end
end
