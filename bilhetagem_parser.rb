require 'nokogiri'
require 'spreadsheet'
require 'byebug'
require 'open-uri'
require 'csv'
require 'roo-xls'

@csv_file = 'bilhetagem_anual.csv'
@year_regexp = /\d{4}/
@uri = 'http://www.prefeitura.sp.gov.br/cidade/secretarias/transportes/institucional/sptrans/acesso_a_informacao/index.php?p=209428'
@href_links = []
@spreadsheets = []

def get_uri
    uri = @uri
end

def set_uri(uri)
    @uri = uri
end

def open_url(uri = get_uri)
    Nokogiri::HTML(open(uri))
end

def catch_years
    all_years = open_url.search('//p//a')
    all_years.each do |yr|
        if yr.children.text.scan(@year_regexp) == [] || yr.children.text.scan(@year_regexp).nil?
            next
        else
            yr_yr = yr.children.text.scan(@year_regexp)
            @href_links << open_url.xpath("//p//a[text() = '#{yr_yr.first}']").attribute('href').value
        end
    end
    @href_links
end

# This is to read the downloaded spreadsheets
def read_spreadsheet
    files = File.join('**', '*.xl*')
    @excel_files = Dir.glob(files)

    if @excel_files.first
        begin
            xls = Roo::Spreadsheet.open(@excel_files.first)
        rescue Exception => msg
            p "This error occurred while opening the Spreadsheet file: #{@excel_files.first} , error: #{msg}"
        end
        xls
    end
end

# This is to delete the downloaded spreadsheet.
def delete_spreadsheet
    File.delete @excel_files.first
rescue Exception => msg
    p "This error occurred while deleting the Spreadsheet file: #{@excel_files.first} , error: #{msg}"
end

# This is to create a CSV file with the data gathered from the spreadsheets
def create_csv
    if File.exist? @csv_file
        begin
            CSV.open(@csv_file, 'a+', converters: :numeric, col_sep: ';', skip_blanks: 'true') do |csv|
                read_spreadsheet.each do |row|
                    row.compact
                    row.map! do |x|
                        x.class.name === 'Float' ? sprintf('%.2f', x) : x
                    end
                    if row.count == 8
                        row = row.insert(7, 0, 0)
                    elsif row.count == 7
                        row = row.insert(6, 0,0,0)
                    elsif row.count == 9
                        row = row.insert(8, 0)
                    end
                    #######################################################################################################
                    # It was a precondition, the document generated must displays just the first header column and         #
                    # not include all the content below the header "2. RECEITA DA COMISSÃO PELA VENDA DE VALE-TRANSPORTE"  #
                    ########################################################################################################
                    next if row.include?('1. RECEITA DA VENDA DE CRÉDITOS / DIA')
                    next if row.include?('DATA')
                    break if row.include?("2. RECEITA DA COMISSÃO PELA VENDA DE VALE-TRANSPORTE")
                    row = '' if row.include?(nil)
                    csv << row
                end
            end
        rescue Exception => msg
            p "This error occurred appending to the Spreadsheet file: #{@csv_file} , error: #{msg}"
        end
        delete_spreadsheet
    else
        begin
            CSV.open(@csv_file, 'wb', converters: :numeric, col_sep: ';', skip_blanks: 'true') do |csv|
                read_spreadsheet.each do |row|
                    row.compact
                    # It was a precondition, the document generated must not include all the content below the sentence "2. RECEITA DA COMISSÃO PELA VENDA DE VALE-TRANSPORTE"
                    next if row.include?('1. RECEITA DA VENDA DE CRÉDITOS / DIA')
                    break if row.include?("2. RECEITA DA COMISSÃO PELA VENDA DE VALE-TRANSPORTE")
                    row.map! do |x|
                        x.class.name === 'Float' ? sprintf('%.2f', x) : x
                    end
                    row = '' if row.include?(nil)
                    csv << row
                end
            end
        rescue Exception => msg
            p "This error occurred while creating the Spreadsheet file: #{@csv_file} , error: #{msg}"
        end
        delete_spreadsheet
    end
end

# This is to download the Spreadsheets from SPTRANS
def download_files
    all_href = open_url.search('//td//a')
    all_href.each do |link|
        l = link.attribute('href').value
        begin
            # To have a better controlling it´s better to refactor it and use another mechanism instead of WGET
            system("wget \"#{l}\" ")
        rescue
            p "Skipping #{l}, This URL it´s not accessible!!!"
        end
        create_csv
    end
end

# This is to run the script
download_files
catch_years.each do |x|
    set_uri(x)
    download_files
end
